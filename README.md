# Benutzer-Dokumentation

für das Medienarchiv der HfG Karlsruhe:  
https://madek.hfg-karlsruhe.de/

Website dieser Dokumentation:  
https://orc-hfg.gitlab.io/user-docs-de/

Allgemeine Informationen zu Madek bei der ZHdK:  
https://medienarchiv.zhdk.ch/hilfe/


## Hilfe zur Hilfe

What is good documentation for software projects?  
https://opensource.com/article/20/4/documentation

The Good Docs Project:  
https://github.com/thegooddocsproject

GitHub Pages:  
https://docs.github.com/en/pages

Markdown:  
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet  
https://www.markdownguide.org/basic-syntax/

Just-the-Docs-Template:  
https://pmarsceill.github.io/just-the-docs/

### GitLab

Register runners  
https://docs.gitlab.com/runner/register/

Run your CI/CD jobs in Docker containers  
https://docs.gitlab.com/ee/ci/docker/using_docker_images.html

Unregister local runners  
https://gitlab.com/gitlab-org/gitlab-runner/-/issues/39

Jeckyll build process: Cache
https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html#build-faster-with-cached-dependencies

GitAhead: stay logged in   
https://stackoverflow.com/a/68739872
  

## Alternativen

Read the Docs:  
https://readthedocs.org/


## Lizenz

Wir folgen den Empfehlungen der Free Software Foundation für Dokumentationen und veröffentlichen diese Dokumentation unter der GNU Free Documentation License (GFDL).

https://www.gnu.org/licenses/fdl-1.3.html  
https://www.gnu.org/licenses/license-recommendations.html