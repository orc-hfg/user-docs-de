---
layout: default
title: Datensatz anlegen
parent: Anleitungen
nav_order: 1
---

# Datensatz anlegen

Test: Screenshot einbinden:

![Ein Screenshot](screenshot.jpg)
*Bildunterschrift*  
{: .fs-2 .text-right }

## Video Test

Direkter Link zum Medieneintrag:  
<video src="https://madek.hfg-karlsruhe.de/entries/9f7391b3-247b-4671-8b3c-193b2be07ecc"></video>

Vertraulicher Link:  
<video src="https://madek.hfg-karlsruhe.de/entries/9f7391b3-247b-4671-8b3c-193b2be07ecc/access/RMPYDS2KC4NJXWFGPP3KJ3J08AAQR35Q"></video>

Embed-Code in html-video-tag:  
<video src="https://madek.hfg-karlsruhe.de/entries/9f7391b3-247b-4671-8b3c-193b2be07ecc/embedded?accessToken=RMPYDS2KC4NJXWFGPP3KJ3J08AAQR35Q&height=360&width=640
"></video>

Embed-Code:  
https://madek.hfg-karlsruhe.de/entries/9f7391b3-247b-4671-8b3c-193b2be07ecc/embedded?accessToken=RMPYDS2KC4NJXWFGPP3KJ3J08AAQR35Q&height=360&width=640

iframe-code:  
<iframe src="https://madek.hfg-karlsruhe.de/entries/9f7391b3-247b-4671-8b3c-193b2be07ecc/embedded?accessToken=RMPYDS2KC4NJXWFGPP3KJ3J08AAQR35Q&amp;height=360&amp;width=640" frameborder="0" width="640" height="360" style="margin:0;padding:0;border:0" allowfullscreen webkitallowfullscreen mozallowfullscreen ></iframe>