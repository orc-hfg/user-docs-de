---
layout: default
title: Die Medienplattform der HfG Karlsruhe
nav_order: 1
---

# Die Medienplattform der HfG Karlsruhe

Wir verwenden die Software Madek als Medienplattform und Archiv für das Medienarchiv der HfG Karlsruhe:  
[https://madek.hfg-karlsruhe.de/](https://madek.hfg-karlsruhe.de/)

Allgemeine Informationen zu Madek bei der ZHdK:  
[https://medienarchiv.zhdk.ch/hilfe/](https://medienarchiv.zhdk.ch/hilfe/)